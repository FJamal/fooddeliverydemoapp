import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import * as Fonts from "expo-font";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import {Home, OrderDelivery, Resturants} from "./screens";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {COLORS, icons} from "./constants";
import Tabs from "./navigation/tabs";

const customFonts = {
  "Roboto-Black" : require("./assets/fonts/Roboto-Black.ttf"),
  "Roboto-Bold" : require("./assets/fonts/Roboto-Bold.ttf"),
  "Roboto-Regular" : require("./assets/fonts/Roboto-Regular.ttf"),
}


const Stack = createStackNavigator()


const MainTabs = ()=> {
  return (
    <NavigationContainer>

      <Stack.Navigator
      initialRouteName = "home"
      screenOptions = {{
        headerShown  : false
      }}>
        <Stack.Screen name = "home" component = {Tabs} />
        <Stack.Screen name = "resturant" component = {Resturants} />
        <Stack.Screen name = "delivery" component = {OrderDelivery} />
      </Stack.Navigator>

    </NavigationContainer>
  )
}

export default class App extends React.Component {
  state = {
    isFontsLoaded : false,
  }

  componentDidMount(){
    this.loadFonts()
  }

  loadFonts = async ()=> {
    await Fonts.loadAsync(customFonts)
    this.setState({isFontsLoaded : true})
  }

  componentWillUnmount(){
    this.setState({isFontsLoaded : false})
  }
  render(){
    if(this.state.isFontsLoaded)
    {
      return(
        <MainTabs/>
      )
    }
    else
    {
      return null

    }
  }



}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
