import MapViewDirections from "react-native-maps-directions";
import React from "react";
import {View, Text, StyleSheet, Image, TouchableOpacity} from "react-native";
import MapView , { Marker, }from "react-native-maps";
import {images,GOOGLE_API_KEY, icons, SIZES , FONTS, COLORS} from "../constants";
import {useNavigation} from "@react-navigation/native";

const RenderMap = (props)=> (
  <View style = {styles.mapContainer}>
    <MapView style = {styles.map} initialRegion ={props.region} ref = {props.reff}>
      <MapViewDirections
        lineDashPattern = {[0]}
        strokeColor = "green"
        strokeWidth = {5}
        origin = {props.fromLocation}
        destination = {props.toLocation}
        apikey = {GOOGLE_API_KEY}
        optimizeWaypoints = {false}
        onReady = {props.onReady}/>
      <DestinatioMarker toLocation = {props.toLocation}/>
      <CarIcon fromLocation = {props.fromLocation} rotation = {props.rotation}/>
    </MapView>

  </View>
)


const CarIcon = (props) => (
  <Marker
    coordinate = {props.fromLocation}
    anchor = {{x : 0.5, y : 0.5}}
    rotation = {props.rotation}
    >
    <Image source = {icons.car} style = {{width : 40, height : 40}}/>
  </Marker>
)


const DestinatioMarker = (props) => {
  console.log("props in DestinatioMarker", props)
  return (
      <Marker  coordinate = {props.toLocation}>
        <View style = {{
            height : 40,
            weight : 40,
            borderRadius : 20,
            justifyContent : "center",
            alignItems : "center",
            backgroundColor : COLORS.white
          }}>
          <View style = {{
              height : 30,
              width : 30,
              justifyContent : "center",
              alignItems : "center",
              borderRadius : 15,
              backgroundColor : COLORS.primary,

            }}>
            <Image
              source = {icons.pin}
              style = {{width : 25, height : 25, tintColor : COLORS.white}}/>
          </View>
        </View>
      </Marker>
  )
}

const DestinationHeader = (props)=> (
  <View style = {styles.destinationcontainer}>
    <View style = {styles.destinationinfoContainer}>
      <Image
        source = {icons.pin}
        style = {{
          width : 30,
          height : 30,
          marginRight : SIZES.padding,
          tintColor : COLORS.primary,
        }}/>
      <View style = {{flex : 1}}>
        <Text style = {{...FONTS.body3}}>{props.streetName}</Text>
      </View>
      <Text style = {{...FONTS.body3}}>{Math.ceil(props.duration)} mins</Text>
    </View>
  </View>
)


const RenderDeliveryInfo = (props)=>{
  const navigation = useNavigation()
  return (
    <View style = {styles.deliveryInfoContainer}>
      <View style = {{
          width : SIZES.width * 0.9,
          paddingVertical : SIZES.padding * 3,
          paddingHorizontal : SIZES.padding * 2,
          borderRadius : SIZES.radius,
          backgroundColor : COLORS.white,
          flexDirection : "row",
          justifyContent : "center",
          alignItems : "center"
        }}>
        <Image source = {props.avatar} style ={{width : 50, height : 50, borderRadius : SIZES.radius, }} resizeMode = "contain"/>
        <View style = {{flexDirection  : "row", flex : 1, marginLeft : SIZES.padding, justifyContent : "space-between"}}>
          {/*Name and ratings*/}
          <View>
            <Text style = {{...FONTS.h4}}>{props.name}</Text>
            <Text style = {{...FONTS.body4, color : COLORS.darkgray}}>{props.resturantName}</Text>
          </View>
          <View style = {{flexDirection : "row"}}>
            <Image style = {{width : 18, height : 18, tintColor : COLORS.primary}} source = {icons.star} />
            <Text style = {{...FONTS.body3, marginLeft : SIZES.padding}}>{props.rating}</Text>
          </View>
        </View>

      </View>
      {/*Buttons*/}
      <View style = {{flexDirection : "row", justifyContent : "space-between", marginTop : SIZES.padding * 2}}>
        <TouchableOpacity style = {{
            height : 50,
            width : SIZES.width * 0.5 - SIZES.padding * 6,
            backgroundColor : COLORS.primary,
            alignItems : "center",
            justifyContent : "center",
            borderRadius : 10,
          }}
          onPress = {props.onPress}>
          <Text style = {{...FONTS.h3, color : COLORS.white}}>Call</Text>
        </TouchableOpacity>
        <TouchableOpacity style = {{
            height : 50,
            width : SIZES.width * 0.5 - SIZES.padding * 6,
            backgroundColor : COLORS.white,
            alignItems : "center",
            justifyContent : "center",
            borderRadius : 10,
            marginLeft : SIZES.padding *2,
          }}
          onPress = {()=>navigation.goBack()}>
          <Text style = {{...FONTS.h3, color : COLORS.black}}>Cancle</Text>
        </TouchableOpacity>

      </View>
    </View>
  )
}


const RenderButtons = (props)=> (
  <View style = {styles.buttonsContainer}>
    <TouchableOpacity style = {{
        width : 60,
        height : 60,
        borderRadius : SIZES.radius,
        backgroundColor : COLORS.white,
        alignItems : "center",
        justifyContent : "center"
      }}
      onPress = {props.zoomIn}>
      <Text style = {{...FONTS.body1}}>+</Text>
    </TouchableOpacity>
    <TouchableOpacity style = {{
        width : 60,
        height : 60,
        borderRadius : SIZES.radius,
        backgroundColor : COLORS.white,
        alignItems : "center",
        justifyContent : "center"
      }}
      onPress = {props.zoomOut}>
      <Text style = {{...FONTS.body1}}>-</Text>
    </TouchableOpacity>
  </View>
)



export default class OrderDeleivery extends React.Component {
  state = {
    resturant : null,
    streetName : "",
    fromLocation : null,
    toLocation : null,
    region : null,
    duration : 0,
    isReady : false,
    angle : 0,
    mapViewRef : React.createRef()
  }

  componentDidMount(){
    let {resturant, currentLocation} = this.props.route.params
    console.log("Resturant in Delivery page:", resturant)
    console.log("currentLocation : ", currentLocation)
    console.log("ComponentDidMount kicked")
    let fromLocation = currentLocation.gps
    let toLocation = resturant.location
    let street = currentLocation.streetName
    //console.log("fromLocation", fromLocation)
    //console.log("toLocation:", toLocation)

    let mapRegion = {
      latitude : (fromLocation.latitude + toLocation.latitude)/2,
      longitude : (fromLocation.longitude + toLocation.longitude)/2,
      latitudeDelta : Math.abs(fromLocation.latitude - toLocation.latitude) * 2,
      longitudeDelta : Math.abs(fromLocation.longitude - toLocation.longitude) * 2,
    }
    //console.log("mapRegion : ", mapRegion)

    this.setState({
      resturant,
      streetName : street,
      fromLocation,
      toLocation,
      region : mapRegion,
      mapLoaded : false
    },
    ()=> {//console.log("STATE after setState : ", this.state)
      this.setState({mapLoaded : true})})

    //console.log("region at the end  of componentDidMount", this.state)
  }

  onReadyForRoute = (result)=> {
    //console.log("Coordinate from onReady is", result)
    this.setState({duration : result.duration})
    let angle
    if(!this.state.isReady)
    {
      //fit the routes into the map
      //console.log("REF is :", this.state.mapViewRef)
      this.state.mapViewRef.current.fitToCoordinates(result.coordinates, {
        edgePadding : {
          right : (SIZES.width/20),
          bottom : (SIZES.height/4),
          left : (SIZES.width/20),
          top : (SIZES.height/8),
        }
      })

      //for angle of car
      if(result.coordinates.length >= 2){
        angle = this.calculateAngle(result.coordinates)
      }

      //reposition the car
      let nextPosition = {
        latitude : result.coordinates[0].latitude,
        longitude : result.coordinates[0].longitude
      }

      this.setState({fromLocation : nextPosition, isReady : true, angle})

    }

  }


  //to calculate the angle of the car icon so that its always facing the route
  calculateAngle = (coordinates)=> {
    let startLat = coordinates[0]["latitude"]
    let startLng = coordinates[0]["longitude"]
    let endLat = coordinates[1]["latitude"]
    let endLng = coordinates[1]["longitude"]
    let dx = endLat - startLat
    let dy = endLng - startLng

    return Math.atan2(dy, dx) * 180 / Math.PI
  }

  //for zoom in
  zoomIn = ()=> {
    let newRegion = {
      latitude : this.state.region.latitude,
      longitude : this.state.region.longitude,
      latitudeDelta : this.state.region.latitudeDelta/2,
      longitudeDelta : this.state.region.longitudeDelta/2,
    }

    this.setState(newRegion)
    this.state.mapViewRef.current.animateToRegion(newRegion, 200)


  }


  //for zoom in
  zoomOut = ()=> {
    let newRegion = {
      latitude : this.state.region.latitude,
      longitude : this.state.region.longitude,
      latitudeDelta : this.state.region.latitudeDelta * 2,
      longitudeDelta : this.state.region.longitudeDelta * 2,
    }

    this.setState(newRegion)
    this.state.mapViewRef.current.animateToRegion(newRegion, 200)


  }

  render(){
    console.log("Render function kicked in")
    console.log("Region in render: ", this.state.region)
    return (
      <View style = {{flex : 1}}>
        {
          this.state.mapLoaded ? (
                          <View style = {{flex: 1}}>

                                <RenderMap
                                    region = {this.state.region}
                                    toLocation = {this.state.toLocation}
                                    fromLocation = {this.state.fromLocation}
                                    onReady = {this.onReadyForRoute}
                                    reff = {this.state.mapViewRef}
                                    rotation = {this.state.angle}/>

                                <DestinationHeader
                                    streetName = {this.state.streetName}
                                    duration = {this.state.duration}/>

                                <RenderDeliveryInfo
                                    avatar = {this.state.resturant.courier.avatar}
                                    name = {this.state.resturant.courier.name}
                                    rating = {this.state.resturant.rating}
                                    resturantName = {this.state.resturant.name}
                                    onPress = {()=>this.props.navigation.navigate("home")} />

                                  <RenderButtons zoomIn = {this.zoomIn} zoomOut = {this.zoomOut}/>
                                </View>) : null
        }


      </View>
    )
  }
}


const styles = StyleSheet.create({
  mapContainer : {
    flex : 1,
    justifyContent : "center",
    alignItems : "center",
  },
  map : {
    width : SIZES.width * 0.9,
    height : SIZES.height * 0.75,
  },
  destinationcontainer : {
    //flexDirection : "row",
    position : "absolute",
    top : 50,
    left : 0,
    right :0,
    height : 50,

    justifyContent : "center",
    alignItems : "center",
    //backgroundColor : "green",
    padding : SIZES.padding,

  },
  destinationinfoContainer : {
    width : SIZES.width * 0.9,
    flexDirection : "row",
    alignItems : "center",
    padding : SIZES.padding,
    backgroundColor : COLORS.white,
    borderRadius : SIZES.radius,
  },
  deliveryInfoContainer : {
    position: "absolute",
    left : 0,
    right : 0,
    bottom : 50,
    justifyContent : "center",
    alignItems : "center",
    borderRadius : SIZES.radius,
    backgroundColor : COLORS.white,
  },
  buttonsContainer : {
    position : "absolute",
    bottom : SIZES.height * 0.35,
    right : SIZES.padding * 2,
    width : 60,
    height : 130,
    justifyContent : "space-between"
  }
})
