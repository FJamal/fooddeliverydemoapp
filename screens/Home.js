    import React, {useState} from "react";
    import {FlatList, View, Text, SafeAreaView, StyleSheet, Image, TouchableOpacity, StatusBar, Platform} from "react-native";
    import {icons, images, COLORS, FONTS, SIZES,} from "../constants";
    import Data from "../assets/data/dummyData";
    import {useNavigation} from "@react-navigation/native";
    var test = Data.restaurantData




    function renderHeader() {
      return(
        <View style = {styles.headerWrapper}>
          <TouchableOpacity style = {{
              width : 50,
              paddingLeft : SIZES.padding * 2,
              backgroundColor : COLORS.white,
              justifyContent : "center",

            }}>
            <Image
              source = {icons.nearby}
              resizeMode = "contain"
              style = {{width : 30, height : 30}}/>

          </TouchableOpacity>
          <View style = {{
              flex : 1,
              backgroundColor : COLORS.white,
              alignItems : "center",
              justifyContent : "center",
              }}>
              <View style = {styles.headerLocation}>
                <Text style = {{...FONTS.h3}}>{Data.initialCurrentLocation.streetName}</Text>

              </View>
        </View>
        <TouchableOpacity style = {styles.basket}>
          <Image
            style = {{width : 30, height : 30}}
            source = {icons.basket}
            resizeMode = "center"/>
        </TouchableOpacity>

      </View>

      )
    }





    function RenderMainCategories() {
      const [selectedCategory, setSelectedCategory] = useState(null)
      const[resturants, setResturants] = useState(Data.restaurantData)


      const onSelectCategory = (category) => {
        console.log(category)
        setSelectedCategory(category)
        //console.log("selectedCategory : ", selectedCategory)

        let restaurantList = Data.restaurantData.filter(item => item.categories.includes(category.id))
        setResturants(restaurantList)
        test = restaurantList
      }


      const renderCategories = (obj)=> {
        return(
          <TouchableOpacity
            style = {[styles.categoryItem, {backgroundColor : (selectedCategory?.id == obj.item.id) ? COLORS.primary: COLORS.white}]}
            onPress = {()=>onSelectCategory(obj.item)}>
            <View style = {[styles.categoryImageContainer,{backgroundColor : (selectedCategory?.id == obj.item.id) ? COLORS.white: COLORS.lightGray }]}>
              <Image
                source = {obj.item.icon}
                style = {{height : "70%", width : "70%"}}
                resizeMode = "contain"/>
            </View>
            <Text style = {{marginTop : SIZES.padding, color : (selectedCategory?.id == obj.item.id) ? COLORS.white: COLORS.black, ...FONTS.body5}}>{obj.item.name}</Text>
          </TouchableOpacity>
        )
      }

      return (
        <View style = {styles.categoriesContainer}>
          <Text style = {{...FONTS.h1}}>Main</Text>
          <Text style = {{...FONTS.h1}}>Categories</Text>


       <View style = {styles.categoryListContainer}>
          <FlatList
            data = {Data.categoryData}
            keyExtractor = {(item) => `${item.id}`}
            horizontal = {true}
            showsHorizontalScrollIndicator = {false}
            renderItem = {renderCategories}
            contentContainerStyle = {{paddingVertical : SIZES.padding * 2}}/>
        </View>
        </View>
      )



      }

      function renderRestaurantList(){
        const[resturants, setResturants] = useState(Data.restaurantData)
        const[categories, setCategories] = useState(Data.categoryData)
        const [currentLocation, setCurrentLocation] = React.useState(Data.initialCurrentLocation)
        const navigation = useNavigation()

        function getCategoryNameById(id){
          let category = categories.filter(a => a.id == id)
          if (category.length > 0)
          {
            return category[0].name
          }
          else
          {
            return ""
          }
        }

        const renderItem = ({item})=>{
          item.id.toString()
          console.log("ITEM : ", item)
          return (
            <TouchableOpacity
              style = {{marginBottom : SIZES.padding * 2}}
              onPress = {()=> navigation.navigate("resturant", {
                item,
                currentLocation,
              })}>

              <View style = {{marginBottom : SIZES.padding}}>
                <Image
                  source = {item.photo}
                  resizeMode = "cover"
                  style = {{width : "100%", height : 200, borderRadius : SIZES.radius}}/>
                <View style = {{
                    bottom : 0,
                    backgroundColor : "pink",
                    position : "absolute",
                    height : 50,
                    width: SIZES.width * 0.3,
                    borderTopRightRadius : SIZES.radius,
                    borderBottomLeftRadius : SIZES.radius,
                    alignItems : "center",
                    justifyContent : "center",

                  }}>
                  <Text style = {{...FONTS.h4}}>{item.duration}</Text>
                </View>
              </View>

              <Text style = {{...FONTS.body2}}>{item.name}</Text>

              <View style = {styles.ratingsContainer}>
                <Image
                  source = {icons.star}
                  style = {{
                    height: 20,
                    width : 20,
                    tintColor : COLORS.primary,
                    marginRight : 10,}}/>
                  <View style = {{marginRight : SIZES.padding * 2}}>
                    <Text style = {{...FONTS.body3}}>{item.rating}</Text>
                  </View>

                  {/*Categories*/}
                  <View style = {{flexDirection : "row", alignItems : "center"}}>
                    {
                      item.categories.map((categoryId)=>{
                        return (
                          <View key = {categoryId} style = {{flexDirection : "row"}}>
                            <Text style = {{...FONTS.body3}}>{getCategoryNameById(categoryId)}</Text>
                            <Text style= {{...FONTS.h3, color : COLORS.darkgray}}> , </Text>
                          </View>
                        )})
                    }
                    <View style = {{flexDirection : "row"}}>
                      {[1,2,3].map(pricerating=>{
                        //console.log("priceRating :", pricerating)
                        //console.log("item.priceRating :", item.priceRating)
                        return (
                          <Text
                            key = {pricerating}
                            style = {{...FONTS.body3, color :  (pricerating <= item.priceRating) ? COLORS.black : "grey"}}>$</Text>
                        )
                      }
                    )}
                    </View>


                  </View>
              </View>
            </TouchableOpacity>
          )
        }
        //console.log("Resturants : ",resturants)

        return(
          <FlatList
            data = {test}
            keyExtractor ={(item)=> `${item.id}`}
            renderItem = {renderItem}
            contentContainerStyle = {{paddingHorizontal : SIZES.padding * 2, paddingBottom : 30}}
            />
        )
      }



    export default function Home () {

        return (
          <SafeAreaView style = {styles.AndroidView}>

            {renderHeader()}
            {RenderMainCategories()}
            {renderRestaurantList()}
          </SafeAreaView>
        )

    }

    const styles = StyleSheet.create({
      AndroidView : {
        flex : 1,
        backgroundColor : Platform.OS == "android" ? COLORS.lightGray1 : "red",
        paddingTop : Platform.OS == "android" ? StatusBar.currentHeight : 0
      },
      shadow : {
        shadowColor : "#000",
        shadowOffset : {
          width : 0,
          height : 3,
        },
        shadowOpacity : 0.1,
        shadowRadius : 3,
        elevation : 1,
      },
      headerWrapper : {
        //width : "100%",
        flexDirection : "row",
        height : 50,

      },
      headerLocation : {
        backgroundColor : COLORS.lightGray3,
        width : "70%",
        alignItems : "center",
        height : "100%",
        justifyContent : "center",
        borderRadius : SIZES.radius,
      },
      basket : {
        width : 50,
        paddingRight : SIZES.padding * 2,
        justifyContent : "center",
      },
      categoriesContainer : {
        padding : SIZES.padding * 2,
      },
      categoryItem : {
        padding : SIZES.padding,
        paddingBottom : SIZES.padding * 2,
        backgroundColor : COLORS.primary,
        borderRadius : SIZES.radius,
        alignItems : "center",
        justifyContent : "center",
        marginRight : SIZES.padding,

      },
      categoryImageContainer : {
        backgroundColor : "white",
        justifyContent : "center",
        alignItems : "center",
        width : 50,
        height : 50,
        borderRadius : 25,
      },
      ratingsContainer : {
        marginTop : SIZES.padding,
        flexDirection : "row",
        alignItems : "center",
      }
    })
