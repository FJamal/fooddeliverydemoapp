// console.log("Restaurant", resturant)
// resturant?.menu.map((item, index)=> (
//     <View
//       key = {`${index}`}
//       style = {{
//         alignItems : "center"
//       }}>
//       <View
//         style = {{
//           height : SIZES.height * 0.35,
//
//         }}>
//         <image
//           source = {item.photo}
//           resizeMode = "cover"
//           style = {{
//             height : "100%",
//             width : SIZES.width
//           }}/>
//       </View>
//     </View>
//   )
// )



import React from "react";
import {View, Text, SafeAreaView, Animated, StyleSheet, Image, TouchableOpacity, StatusBar, Platform} from "react-native";
import {icons,SIZES,COLORS, FONTS, images} from "../constants";
import {isIphoneX} from "react-native-iphone-x-helper";
import {useNavigation} from "@react-navigation/native";




const Resturants = ({navigation, route})=> {
  const scrollX = new Animated.Value(0)
  const[resturant, setResturants] = React.useState(null)
  const[currentLocation, setCurrentLocation] = React.useState(null)
  const[orderItems, setOrderItems] = React.useState([])

  React.useEffect(() => {
      let {item, currentLocation} = route.params
      setResturants(item)
      setCurrentLocation(currentLocation)
  })


function renderHeader(){
  return (
    <View style = {styles.headerContainer}>
      <TouchableOpacity
        style = {{
          width : 50,
          justifyContent : "center",
          paddingLeft : SIZES.padding * 2}}
        onPress = {()=> navigation.goBack()}>
        <Image
          source = {icons.back}
          style = {{width: 30, height : 30}}
          resizeMode = "contain"
          />
      </TouchableOpacity>
      <View style = {{
          flex : 1,
          //backgroundColor : "red",
          justifyContent : "center",
          alignItems : "center",
        }}>

        <View style = {{
            alignItems : "center",
            justifyContent : "center",
            height : 50,
            paddingHorizontal : SIZES.padding * 3,
            backgroundColor : COLORS.lightGray3,
            borderRadius : SIZES.radius,

          }}>
          <Text style = {{...FONTS.h3}}>{resturant?.name}</Text>

        </View>

      </View>
      <TouchableOpacity style = {styles.headerMenucontaioner}>
        <Image
          source = {icons.list}
          style = {{width : 30, height : 30}}
          resizeMode = "contain"/>
      </TouchableOpacity>
    </View>
  )
}

function editOrder(action, menuId, price)
{
  console.log("editorder working")
  let orderList = orderItems.slice()
  let item = orderList.filter(a=> a.menuId == menuId)
  if(action == "+")
  {

     if(item.length > 0)
     {
       item[0].quantity += 1,
       item[0].price += item[0].price
       item[0].total = item[0].quantity * item[0].price
     }
     else
     {
       orderList.push(
         {
           menuId,
           price,
           quantity : 1,
           total : price
         }
       )
     }
     setOrderItems(orderList)
     console.log("ORDERLIST IS NOW:", orderItems)
  }
  else
  {
    if(item.length > 0)
    {
      if(item[0]?.quantity > 0)
      {
        item[0].quantity -= 1,
        item[0].total = item[0].quantity * item[0].price
      }
    }
    setOrderItems(orderList)
  }
  //return orderList
}

function getQuantity(menuId)
{
  let orderItem = orderItems.filter(a => a.menuId == menuId)
  if(orderItem.length > 0)
  {
    return orderItem[0].quantity
  }
}

function getCartItemsCount(){
  let count = orderItems.reduce((a, b) => a + (b.quantity || 0), 0)
  console.log("Count is", count)
  return count
}

function getTotal(){
  let total = orderItems.reduce((a, b) => a + b.total, 0)
  return total
}

function renderFoodInfo(){
  return (
    <Animated.ScrollView
      horizontal
      pagingEnabled
      scrollEventThrottle = {16}
      snapToAlignment = "center"
      showsHorizontalScrollIndicator = {false}
      onScroll={Animated.event([
          { nativeEvent: { contentOffset: { x: scrollX, } } }
      ], { useNativeDriver: false })}
      >
      {
        //console.log("Restaurant", resturant)
        resturant?.menu.map((item, index) => (
            <View
              key = {`${index}`}
              style = {{
                alignItems : "center"
              }}>
              <View
                style = {{
                  height : SIZES.height * 0.35,

                }}>
                <Image
                  source = {item.photo}
                  resizeMode = "cover"
                  style = {{
                    height : "100%",
                    width : SIZES.width
                  }}/>

                {/*Quantity*/}
                <View style = {styles.quantityContainer }>
                  <TouchableOpacity onPress = {()=> editOrder("-", item.menuId, item.price)}style = {styles.SignContainer}>
                    <Text style = {{...FONTS.body1}}>-</Text>
                  </TouchableOpacity>
                  <View style = {styles.quantityNumberContainer}>
                    <Text style = {{...FONTS.h1}}>{getQuantity(item.menuId) || "0"}</Text>
                  </View>
                  <TouchableOpacity
                    style = {[styles.SignContainer, {borderTopLeftRadius : 0, borderBottomLeftRadius : 0, borderTopRightRadius : 25, borderBottomRightRadius : 25,}]}
                    onPress = {()=> editOrder("+", item.menuId, item.price)}>

                    <Text style = {{...FONTS.body1}}>+</Text>
                  </TouchableOpacity>
                </View>
              </View>

              {/*Name and Description*/}
              <View style = {styles.nameDescriptionContainer}>
                <Text style = {{textAlign : "center",  ...FONTS.h2}}>{item.name} - $ {item.price.toFixed(2)}</Text>
                <View style = {{marginTop : 10}}>
                  <Text style = {{...FONTS.body3}}>{item.description}</Text>
                </View>
                <View style = {styles.cailoriesContainer}>
                  <Image source = {icons.fire} style = {{width : 30, height : 30, marginRight : 10}}/>
                  <Text style = {{...FONTS.body2, color : COLORS.darkgray}}>{item.calories.toFixed(2)} cal</Text>
                </View>
              </View>
            </View>
          )
        )
      }
    </Animated.ScrollView>
  )
}

function renderDots(){
  const dotPosition = Animated.divide(scrollX, SIZES.width)
  console.log("scrollX : ", scrollX);
  console.log("dotPosition", dotPosition)
  return (
    <View style = {{height : 30}}>
      <View style = {{height : 10, flexDirection : "row", justifyContent : "center", alignItems : "center"}}>
        {
          resturant?.menu.map((item, index) => {
            console.log("index:", index)
            const opacity = dotPosition.interpolate({
              inputRange : [index -1 , index, index + 1],
              outputRange : [0.3 , 1 , 0.3],
            })

            const dotSize = dotPosition.interpolate({
              inputRange : [index - 1, index, index + 1],
              outputRange : [SIZES.base * 0.8, 10, SIZES.base * 0.8]
            })

            const dotColor = dotPosition.interpolate({
              inputRange : [index - 1, index, index + 1],
              outputRange : ["rgb(255,20,147)", COLORS.primary, "rgb(255,20,147)"]
            })
            return (
              <Animated.View
                key = {index}
                opacity = {opacity}
                style = {{
                  width : dotSize,
                  height : dotSize,
                  backgroundColor : dotColor,
                  borderRadius : SIZES.radius,
                  marginRight : 10,
                  //bottom : 50
                }}>
              </Animated.View>
            )
          })
        }
      </View>
    </ View>
  )
}


function renderOrder(){
  return (
    <View >
      {
        renderDots()
      }
      <View style = {styles.orderInfoContainer}>
        <View style = {{
            flexDirection : "row",
            justifyContent : "space-between",
            paddingVertical : SIZES.padding * 2,
            paddingHorizontal : SIZES.padding * 3,
            borderBottomColor : COLORS.lightGray3,
            borderBottomWidth : 1,
          }}>
          <Text style = {{...FONTS.h3}}>{getCartItemsCount()} Items in cart</Text>
          <Text style = {{...FONTS.h3}}>$ {getTotal() || 0}</Text>
        </View>

        <View style = {{
            flexDirection : "row",
            paddingVertical : SIZES.padding * 2,
            paddingHorizontal : SIZES.padding * 3,
            justifyContent : "space-between",
          }}>
          <View style = {{flexDirection : "row"}}>
            <Image source = {icons.pin} style = {{width : 20, height : 20, tintColor : COLORS.darkgray}} resizeMode = "contain"/>
            <Text style = {{marginLeft : SIZES.padding, ...FONTS.h4}}>Location</Text>
          </View>
          <View style = {{flexDirection : "row"}}>
            <Image
              source = {icons.master_card}
              resizeMode = "contain"
              style = {{
                height : 20,
                width : 20,
                tintColor : COLORS.darkgray,
              }}/>
            <Text style = {{marginLeft : 10, ...FONTS.h4}}>8888</Text>
          </View>
        </View>

        {/*Order Button*/}
        <View style = {{
            justifyContent : "center",
            alignItems : "center",
            padding : 20,
          }}>
          <TouchableOpacity style = {{
              alignItems : "center",
              width : SIZES.width * 0.9,
              borderRadius : SIZES.radius,
              backgroundColor : COLORS.primary,
              padding : SIZES.padding,
            }}
            onPress = {() => navigation.navigate("delivery", {
              resturant, currentLocation
            })}>
            <Text style = {{color : COLORS.white, ...FONTS.h2}}>Order</Text>
          </TouchableOpacity>
        </View>
      </View>
      {
        (isIphoneX) ?(
            <View style = {{
                position : "absolute",
                bottom : -34,
                left : 0,
                right : 0,
                height : 34,
                backgroundColor : COLORS.white
              }}>
            </View>
          ) : null

      }
    </View>

  )
}



  return (
    <SafeAreaView style = {styles.AndroidView}>
      {renderHeader()}
      {renderFoodInfo()}
      {renderOrder()}
    </SafeAreaView>

  )
}

const styles = StyleSheet.create({
  AndroidView : {
    flex : 1,
    paddingTop : Platform.OS == "android" ? StatusBar.currentHeight : 0,
    //alignItems : "center"
  },
  headerContainer : {
    flexDirection : "row",
  },
  headerMenucontaioner : {
    width : 50,
    paddingRight : SIZES.padding * 2,
    justifyContent : "center",
    //alignItems : "center"
  },
  quantityContainer : {
    width : SIZES.width,
    position : "absolute",
    flexDirection : "row",
    height : 50,
    borderRadius : SIZES.radius,
    //backgroundColor : "red",
    alignItems : "center",
    justifyContent : "center",
    bottom :-20
  },
  SignContainer : {
    width : 50,
    alignItems : "center",
    justifyContent : "center",
    borderTopLeftRadius : 25,
    borderBottomLeftRadius : 25,
    backgroundColor : "pink",

  },
  quantityNumberContainer : {
    width : 50,
    backgroundColor : "pink",
    alignItems : "center",
    justifyContent : "center",
  },
  nameDescriptionContainer : {
    width: SIZES.width,
    marginTop : SIZES.padding * 2,
    alignItems : "center",
    marginTop : 15,
  },
  cailoriesContainer : {
    marginTop : SIZES.padding * 2,
    flexDirection : "row",
    //justifyContent : "center",
    alignItems : "center",
    //backgroundColor : "red",

  },
  orderInfoContainer : {
    borderTopLeftRadius : 40,
    borderTopRightRadius : 40,
    backgroundColor : COLORS.white,
  }

})

export default Resturants;
